
if (TESTS)

link_directories(${CMAKE_BINARY_DIR}/src)

include_directories(
    ${TUFAO_INCLUDE_DIR}
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_BINARY_DIR}/tests
    ${QJSONRPC_INCLUDE_DIRS})

include(${QT_USE_FILE})

link_libraries(${QT_LIBRARIES} ${QJSONRPC_LIBRARIES} ${TUFAO_LIBRARIES} QJsonRpcTufao)
add_executable(tst_qjsonrpctufaoserver tst_qjsonrpctufaoserver.cpp)
qt_use_modules(tst_qjsonrpctufaoserver Core Test)

endif (TESTS)
