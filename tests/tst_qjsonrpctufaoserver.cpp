/*
** Copyright (C) 2015 Fargier Sylvain <fargier.sylvain@free.fr>
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** tst_jsonrpctufaoserver.cpp
**
**        Created on: mars 20, 2015
**   Original Author: Fargier Sylvain <fargier.sylvain@free.fr>
**
*/

#include <QtTest/QtTest>

#include <Tufao/HttpServer>
#include <qjsonrpcservice.h>
#include <qjsonrpchttpclient.h>

#include "QJsonRpcTufaoServer.hpp"


class TestQJsonRpcTufaoServer : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void test();
};

class TestService : public QJsonRpcService
{
    Q_OBJECT
    Q_CLASSINFO("serviceName", "service")
public:
    TestService(QObject *parent = 0)
        : QJsonRpcService(parent)
    {}

public Q_SLOTS:
    QString multipleParam(const QString &first,
                          const QString &second,
                          const QString &third) const
    {
        return first + second + third;
    }

};

void TestQJsonRpcTufaoServer::test()
{
    Tufao::HttpServer server;
    QJsonRpcTufaoServer rpcServer;

    QObject::connect(&server, &Tufao::HttpServer::requestReady,
                     &rpcServer, &QJsonRpcTufaoServer::handleRequest);
    rpcServer.addService(new TestService);

    QVERIFY(server.listen(QHostAddress::LocalHost, 8118));

    QJsonRpcHttpClient client;
    client.setEndPoint("http://127.0.0.1:8118");

    QJsonArray params;
    params.append(QString("re"));
    params.append(QString("pl"));
    params.append(QString("y"));
    QJsonRpcMessage request = QJsonRpcMessage::createRequest("service.multipleParam", params);
    QJsonRpcMessage response = client.sendMessageBlocking(request);
    QVERIFY(response.type() != QJsonRpcMessage::Error);
    QCOMPARE(request.id(), response.id());
    QCOMPARE(response.result().toString(), QString("reply"));

    /* failure test */
    request = QJsonRpcMessage::createRequest("service.doesnotexist");
    response = client.sendMessageBlocking(request);
    QVERIFY(response.type() == QJsonRpcMessage::Error);
    QCOMPARE(static_cast<QJsonRpc::ErrorCode>(response.errorCode()), QJsonRpc::MethodNotFound);
}

QTEST_MAIN(TestQJsonRpcTufaoServer)
#include "tst_qjsonrpctufaoserver.moc"

