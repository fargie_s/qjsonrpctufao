if (WIN32)
    add_custom_target(PackBundle
            COMMAND ${CMAKE_COMMAND} ${CMAKE_SOURCE_DIR} -P ${CMAKE_BINARY_DIR}/PackBundle.cmake)
    set(CPACK_INSTALL_COMMANDS
        "${CMAKE_COMMAND} ${CMAKE_SOURCE_DIR} -DPACK_INSTALLED=ON -P ${CMAKE_BINARY_DIR}/PackBundle.cmake")

    set(CMAKE_INSTALL_BINDIR "bin" CACHE PATH "")
    set(CMAKE_INSTALL_LIBDIR "bin" CACHE PATH "")
    set(CMAKE_INSTALL_INCLUDEDIR "include" CACHE PATH "")
    set(CMAKE_INSTALL_DATADIR "data" CACHE PATH "")
    set(CMAKE_INSTALL_DATAROOTDIR "data" CACHE PATH "")
    set(CMAKE_INSTALL_WWWDIR "www")
else (WIN32)
    include(GNUInstallDirs)
    set(CMAKE_INSTALL_WWWDIR ${CMAKE_INSTALL_DATAROOTDIR}/${CMAKE_PROJECT_NAME})
endif (WIN32)
