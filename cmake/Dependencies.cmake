
option(IGNORE_QT5 "Ignore Qt5 install and compile with Qt4" OFF)
if (NOT IGNORE_QT5)
    find_package(Qt5Core QUIET)
endif (NOT IGNORE_QT5)

if (Qt5Core_FOUND)
    message(STATUS "Using Qt ${Qt5Core_VERSION_STRING}")
    find_package(Qt5Network REQUIRED)
    if(TESTS)
        find_package(Qt5Test REQUIRED)
    endif(TESTS)

    set(QT_LIBRARIES
        ${Qt5Core_LIBRARIES}
        ${Qt5Network_LIBRARIES}
        ${QtTest_LIBRARIES}
        )
    set(QT_INCLUDES
        ${Qt5Core_INCLUDE_DIRS}
        ${Qt5Network_INCLUDE_DIRS}
        ${QtTest_INCLUDE_DIRS}
        )
    set(QTVERSION "${Qt5Core_VERSION_STRING}")

    set(QT_USE_FILE "${CMAKE_CURRENT_LIST_DIR}/ECMQt4To5Porting.cmake")
    include("${CMAKE_CURRENT_LIST_DIR}/ECMQt4To5Porting.cmake")

    macro(qt_use_modules)
        qt5_use_modules(${ARGN})
    endmacro()
else (Qt5Core_FOUND)
    set(QT_REQ QtCore)
    set(QT_REQ ${QT_REQ} QtNetwork)
    if(TESTS)
        set(QT_REQ ${QT_REQ} QtTest)
    endif(TESTS)

    find_package(Qt4 4.7 REQUIRED ${QT_REQ})

    function(qt_use_modules TARGET MODULES)
        list(REMOVE_ITEM MODULES "Widgets")
        if (MODULES)
            qt4_use_modules(${TARGET} ${MODULES})
        endif (MODULES)
    endfunction()
endif (Qt5Core_FOUND)

include(CheckFunctionExists)

find_package(CXX11 REQUIRED)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX11_FLAGS}")

# Find Qt's private headers
find_path(QTPRIV_INCLUDES private/qobject_p.h
        PATHS ${QT_INCLUDES}
        PATH_SUFFIXES "${QTVERSION}/QtCore/")
if (NOT QTPRIV_INCLUDES)
    message(SEND_ERROR "Qt private headers not found")
endif (NOT QTPRIV_INCLUDES)

include(Dependencies_qjsonrpc)
include(Dependencies_tufao)

