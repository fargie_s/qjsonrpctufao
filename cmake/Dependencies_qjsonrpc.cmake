
include(FindPkgConfig)
pkg_check_modules(QJSONRPC qjsonrpc)
if (NOT QJSONRPC_FOUND)
    find_library(QJSONRPC_LIBRARIES NAMES qjsonrpc
        PATHS ${SYSROOT})
    find_path(QJSONRPC_INCLUDE_DIRS "qjsonrpcservice.h"
        PATHS ${SYSROOT} PATH_SUFFIXES "qjsonrpc")
    if (NOT QJSONRPC_LIBRARIES OR NOT QJSONRPC_INCLUDE_DIRS)
        message(SEND_ERROR "qjsonrpc library not found")
    endif (NOT QJSONRPC_LIBRARIES OR NOT QJSONRPC_INCLUDE_DIRS)
endif (NOT QJSONRPC_FOUND)

