/**
 ** Copyright (C) 2014 fargie_s
 **
 ** This software is provided 'as-is', without any express or implied
 ** warranty.  In no event will the authors be held liable for any damages
 ** arising from the use of this software.
 **
 ** Permission is granted to anyone to use this software for any purpose,
 ** including commercial applications, and to alter it and redistribute it
 ** freely, subject to the following restrictions:
 **
 ** 1. The origin of this software must not be misrepresented; you must not
 **    claim that you wrote the original software. If you use this software
 **    in a product, an acknowledgment in the product documentation would be
 **    appreciated but is not required.
 ** 2. Altered source versions must be plainly marked as such, and must not be
 **    misrepresented as being the original software.
 ** 3. This notice may not be removed or altered from any source distribution.
 **
 ** QJsonRpcTufaoServer_p.hpp
 **
 **        Created on: 15/06/2014
 **   Original Author: fargie_s
 **
 **/

#ifndef QJsonRpcTufaoSERVER_P_HPP
#define QJsonRpcTufaoSERVER_P_HPP

#include <QBuffer>

#include <private/qjsonrpcabstractserver_p.h>
#include <private/qjsonrpcsocket_p.h>
#include <qjsonrpcsocket.h>

#include "QJsonRpcTufaoServer.hpp"

class HttpJsonRequestPrivate;
class HttpJsonRequest : public QJsonRpcSocket
{
    Q_OBJECT
    Q_PROPERTY(QString username READ username);
public:
    HttpJsonRequest(
            Tufao::HttpServerRequest *request,
            Tufao::HttpServerResponse *repsonse,
            QObject *parent);
    ~HttpJsonRequest();

    Tufao::HttpServerRequest *request() const;
    Tufao::HttpServerResponse *response() const;

    QString username() const;

public slots:
    void notify(const QJsonRpcMessage &msg);

private:
    Q_DECLARE_PRIVATE(HttpJsonRequest)
    Q_DISABLE_COPY(HttpJsonRequest)

    Q_PRIVATE_SLOT(d_func(), void _q_processIncomingData())
    Q_PRIVATE_SLOT(d_func(), void _q_requestEnded())
};

class HttpJsonRequestPrivate : public QJsonRpcSocketPrivate
{
    Q_DECLARE_PUBLIC(HttpJsonRequest)

public:
    HttpJsonRequestPrivate(
            Tufao::HttpServerRequest *request,
            Tufao::HttpServerResponse *response) :
        request(request), response(response)
    {
        device = &temp;
    }

    void _q_processIncomingData() {}
    void _q_requestEnded();

    QBuffer temp;
    QPointer<Tufao::HttpServerRequest> request;
    QPointer<Tufao::HttpServerResponse> response;
};

class QJsonRpcTufaoServerPrivate : public QJsonRpcAbstractServerPrivate
{
    Q_DECLARE_PUBLIC(QJsonRpcTufaoServer)
public:
    QJsonRpcTufaoServerPrivate() {}

    void _q_processIncomingConnection() {}
    void _q_clientDisconnected() {}

    typedef QMap<QObject *, QPointer<HttpJsonRequest> > ReqMap;
    ReqMap m_pending;
};

#endif // QJsonRpcTufaoSERVER_P_HPP
