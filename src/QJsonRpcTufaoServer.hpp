/**
 ** Copyright (C) 2014 fargie_s
 **
 ** This software is provided 'as-is', without any express or implied
 ** warranty.  In no event will the authors be held liable for any damages
 ** arising from the use of this software.
 **
 ** Permission is granted to anyone to use this software for any purpose,
 ** including commercial applications, and to alter it and redistribute it
 ** freely, subject to the following restrictions:
 **
 ** 1. The origin of this software must not be misrepresented; you must not
 **    claim that you wrote the original software. If you use this software
 **    in a product, an acknowledgment in the product documentation would be
 **    appreciated but is not required.
 ** 2. Altered source versions must be plainly marked as such, and must not be
 **    misrepresented as being the original software.
 ** 3. This notice may not be removed or altered from any source distribution.
 **
 ** QJsonRpcTufaoServer.hpp
 **
 **        Created on: 15/06/2014
 **   Original Author: fargie_s
 **
 **/

#ifndef QJsonRpcTufaoSERVER_HPP
#define QJsonRpcTufaoSERVER_HPP

#include <QObject>
#include <QPointer>
#include <QMap>

#include <qjsonrpcabstractserver.h>

#include <Tufao/HttpServerRequest>
#include <Tufao/HttpServerResponse>
#include <Tufao/AbstractHttpServerRequestHandler>

class QJsonRpcTufaoServerPrivate;
class QJsonRpcTufaoServer :
        public QJsonRpcAbstractServer,
        public Tufao::AbstractHttpServerRequestHandler
{
    Q_OBJECT
public:
    explicit QJsonRpcTufaoServer(QObject *parent = 0);

    virtual QString errorString() const;

    bool handleRequest(Tufao::HttpServerRequest &request,
                       Tufao::HttpServerResponse &response);

protected:
    Q_DECLARE_PRIVATE(QJsonRpcTufaoServer)
    Q_DISABLE_COPY(QJsonRpcTufaoServer)
};

#endif // QJsonRpcTufaoSERVER_HPP
