/**
 ** Copyright (C) 2014 fargie_s
 **
 ** This software is provided 'as-is', without any express or implied
 ** warranty.  In no event will the authors be held liable for any damages
 ** arising from the use of this software.
 **
 ** Permission is granted to anyone to use this software for any purpose,
 ** including commercial applications, and to alter it and redistribute it
 ** freely, subject to the following restrictions:
 **
 ** 1. The origin of this software must not be misrepresented; you must not
 **    claim that you wrote the original software. If you use this software
 **    in a product, an acknowledgment in the product documentation would be
 **    appreciated but is not required.
 ** 2. Altered source versions must be plainly marked as such, and must not be
 **    misrepresented as being the original software.
 ** 3. This notice may not be removed or altered from any source distribution.
 **
 ** QJsonRpcTufaoServer.cpp
 **
 **        Created on: 15/06/2014
 **   Original Author: fargie_s
 **
 **/

#include <qjsonrpcservice.h>
#include <Tufao/Headers>
#include <QDebug>

#include "QJsonRpcTufaoServer_p.hpp"

static Tufao::Headers jsonHeadersInit()
{
    Tufao::Headers h;
    h.insert("Content-Type", "application/json-rpc");
    return h;
}

static const Tufao::Headers s_jsonHeaders = jsonHeadersInit();

HttpJsonRequest::HttpJsonRequest(
        Tufao::HttpServerRequest *request,
        Tufao::HttpServerResponse *response,
        QObject *parent) :
    QJsonRpcSocket(*new HttpJsonRequestPrivate(request, response), parent)
{
    connect(request, SIGNAL(end()),
            this, SLOT(_q_requestEnded()));
}

HttpJsonRequest::~HttpJsonRequest()
{
    Q_D(HttpJsonRequest);
    if (d->response)
    {
        d->response->writeHead(Tufao::HttpResponseStatus::INTERNAL_SERVER_ERROR,
                           "HttpJsonRequest deleted");
        d->response->end("Internal error");
    }
}

Tufao::HttpServerRequest *HttpJsonRequest::request() const
{
    const Q_D(HttpJsonRequest);
    return d->request.data();
}

Tufao::HttpServerResponse *HttpJsonRequest::response() const
{
    const Q_D(HttpJsonRequest);
    return d->response.data();
}

QString HttpJsonRequest::username() const
{
    const Q_D(HttpJsonRequest);
    if (d->request)
        return d->request->property("username").toString();
    else
        return QString();
}

void HttpJsonRequest::notify(const QJsonRpcMessage &msg)
{
    Q_D(HttpJsonRequest);
    if (!d->response)
        return;

    QJsonDocument doc = QJsonDocument(msg.toObject());

    QByteArray data = doc.toJson();

    d->response->writeHead(Tufao::HttpResponseStatus::OK, s_jsonHeaders);
    d->response->end(data);
    d->response.clear();
    deleteLater();
}

QJsonRpcTufaoServer::QJsonRpcTufaoServer(QObject *parent) :
    QJsonRpcAbstractServer(*new QJsonRpcTufaoServerPrivate, parent)
{
}

QString QJsonRpcTufaoServer::errorString() const
{
    return QString();
}

bool QJsonRpcTufaoServer::handleRequest(
        Tufao::HttpServerRequest &request,
        Tufao::HttpServerResponse &response)
{
    Q_D(QJsonRpcTufaoServer);
    /* FIXME: check content-type in headers */
    HttpJsonRequest *sock = new HttpJsonRequest(&request, &response, this);

    connect(sock, SIGNAL(messageReceived(QJsonRpcMessage)),
            this, SLOT(_q_processMessage(QJsonRpcMessage)));
    connect(&request, SIGNAL(destroyed()),
            sock, SLOT(deleteLater()));
    return true;
}


void HttpJsonRequestPrivate::_q_requestEnded()
{
    Q_Q(HttpJsonRequest);
    if (!request)
        q->deleteLater();

    QJsonRpcMessage msg(request->readBody());
    if (msg.isValid())
        emit q->messageReceived(msg);
    else
    {
        if (response)
        {
            response->writeHead(Tufao::HttpResponseStatus::BAD_REQUEST,
                               "Invalid json-rpc request");
            response->end("Invalid json-rpc request");
            response.clear();
        }
        q->deleteLater();
    }
}
